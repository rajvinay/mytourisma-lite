package com.ftl.tourisma;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.ftl.tourisma.activity.MainActivity;
import com.ftl.tourisma.activity.NoInternet;
import com.ftl.tourisma.custom_views.NormalTextView;
import com.ftl.tourisma.database.DBAdapter;
import com.ftl.tourisma.database.Language;
import com.ftl.tourisma.models.SliderModel;
import com.ftl.tourisma.postsync.PostSync;
import com.ftl.tourisma.postsync.post_sync;
import com.ftl.tourisma.utils.CommonClass;
import com.ftl.tourisma.utils.Constants;
import com.github.paolorotolo.appintro.util.AppConstants;
import com.google.gson.Gson;
import com.pixplicity.easyprefs.library.Prefs;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import javax.net.ssl.HttpsURLConnection;

//import android.util.Log;

/**
 * Created by fipl11111 on 22-Feb-16.
 */
public class LanguageFragmentActivity extends FragmentActivity implements View.OnClickListener, post_sync.ResponseHandler, BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {

    public static final String STATUS = "status";
    public static final int OK = 200;
    //    private static final Integer[] IMAGES = {R.drawable.imageone, R.drawable.imagetwo, R.drawable.imagethree, R.drawable.imagefour, R.drawable.imagefive, R.drawable.imagesix, R.drawable.imageseven, R.drawable.imageeight, R.drawable.imagenine, R.drawable.imageten};
    private static final String TAG = LanguageFragmentActivity.class.getSimpleName();
    private static SliderLayout slider;
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    Animation animFadeIn, animFadeOut;
    Handler handler = new Handler();
    // slide handler view thread
    Handler handlerl = new Handler();
    private ArrayList<SliderModel> ImagesArray = new ArrayList<SliderModel>();
    Runnable Update = new Runnable() {
        public void run() {
            if (currentPage == ImagesArray.size()) {
                currentPage = 0;
            }
//            mPager.setCurrentItem(currentPage++, true);
        }
    };
    private PagerIndicator custom_indicator;
    private NormalTextView button_bt_english, button_bt_russian, button_bt_arabic;
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;
    private DBAdapter dbAdapter;
    private Language language;
    private NormalTextView txtChooseLanguage;
    Runnable runnableIn = new Runnable() {
        @Override
        public void run() {
            txtChooseLanguage.startAnimation(animFadeIn);
            handler.removeCallbacks(runnableIn);
            handler.postDelayed(runnableOut, 3000);
        }
    };
    Runnable runnableOut = new Runnable() {
        @Override
        public void run() {
            txtChooseLanguage.startAnimation(animFadeOut);
            handler.removeCallbacks(runnableOut);
            handler.postDelayed(runnableIn, 200);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);


//        Constants.homepage = "homepage";

        dbAdapter = new DBAdapter(this);
        mPreferences = getSharedPreferences(Constants.mPref, 0);
        mEditor = mPreferences.edit();

        mEditor.putString("language", "6").commit();
        mEditor.putString("Lan_Id", "6").commit();

        downloadSliderImages();
        languageCall();

        initialisation();


        button_bt_english.setOnClickListener(this);
        button_bt_arabic.setOnClickListener(this);
        button_bt_russian.setOnClickListener(this);

// load animations
        animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.fade_in);
        animFadeOut = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.fade_out);
// set animation listeners
        animFadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                if (txtChooseLanguage.getTag().equals("EN")) {
                    txtChooseLanguage.setTag("RU");
                    txtChooseLanguage.setText(R.string.choose_language_string_ru);
                } else if (txtChooseLanguage.getTag().equals("AR")) {
                    txtChooseLanguage.setTag("EN");
                    txtChooseLanguage.setText(R.string.choose_language_string_en);
                } else if (txtChooseLanguage.getTag().equals("RU")) {
                    txtChooseLanguage.setTag("AR");
                    txtChooseLanguage.setText(R.string.choose_language_string_ar);
                }
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        animFadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                txtChooseLanguage.setText("");
//                txtChooseLanguage.startAnimation(animFadeIn);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        txtChooseLanguage.setTag("EN");
        handler.postDelayed(runnableIn, 3000);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        int currentOrientation = getResources().getConfiguration().orientation;
        if (newConfig.orientation == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT ||
                newConfig.orientation == ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT) {
            super.onConfigurationChanged(newConfig);
        }


    }

    private void initialisation() {

        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
//                handlerl.post(Update);
                handlerl.postDelayed(Update, 5000);
            }
        }, 5000, 5000);

        txtChooseLanguage = (NormalTextView) findViewById(R.id.txtChooseLanguage);
        button_bt_english = (NormalTextView) findViewById(R.id.button_bt_english);
        button_bt_russian = (NormalTextView) findViewById(R.id.button_bt_russian);
        button_bt_arabic = (NormalTextView) findViewById(R.id.button_bt_arabic);





        if (Constants.mLanguage == 7) {
            mEditor.putString("language", "7").commit();
            mEditor.putString("Lan_Id", "7").commit();

//
        } else if (Constants.mLanguage == 8) {
            mEditor.putString("language", "8").commit();
            mEditor.putString("Lan_Id", "8").commit();

//
        } else if (Constants.mLanguage == 6) {
            mEditor.putString("language", "6").commit();
            mEditor.putString("Lan_Id", "6").commit();

        }
    }

    @Override
    public void onClick(View v) {
        if (v == button_bt_arabic) {
            Intent mIntent = new Intent(LanguageFragmentActivity.this, MainActivity.class);
            startActivity(mIntent);
            finish();
            AppConstants.language = "arabic";
            Constants.language = "arabic";
            Prefs.putString(Constants.lang_count, "Arabic");
            new SendPostRequest().execute();
            mEditor.putString("language", "8").commit();
            mEditor.putString("Lan_Id", "8").commit();
            Constants.mLanguage = 8;

//
        } else if (v == button_bt_russian) {
            Intent mIntent = new Intent(LanguageFragmentActivity.this, MainActivity.class);
            startActivity(mIntent);
            finish();
            AppConstants.language = "russian";
            Constants.language = "russian";
            Prefs.putString(Constants.lang_count, "Russian");
            new SendPostRequest().execute();
            mEditor.putString("language", "7").commit();
            mEditor.putString("Lan_Id", "7").commit();
            Constants.mLanguage = 7;

//
        } else if (v == button_bt_english) {
            Intent mIntent = new Intent(LanguageFragmentActivity.this, MainActivity.class);
            startActivity(mIntent);
            finish();
            AppConstants.language = "english";
            Constants.language = "english";
            Prefs.putString(Constants.lang_count, "English");
            new SendPostRequest().execute();
            mEditor.putString("language", "6").commit();
            mEditor.putString("Lan_Id", "6").commit();
            Constants.mLanguage = 6;

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void downloadSliderImages() {
        if (CommonClass.hasInternetConnection(getApplicationContext())) {
            String url = "http://54.93.117.123/analytic-cms/API/get_slider_image_for_stand/" + Prefs.getString(Constants.stand_id, "");
            String json = "";
            new PostSync(getApplicationContext(), "sliderImagesResponse", LanguageFragmentActivity.this).execute(url, json);
        } else {
            Intent intent = new Intent(getApplicationContext(), NoInternet.class);
            startActivity(intent);
        }
    }

    /*public void sliderImagesResponse(String resultString) {
        Gson gson = new Gson();
        ImagesArray.clear();
        try {
            JSONObject jsonObject = new JSONObject(resultString);
            JSONArray slider_images_jsonArray = jsonObject.getJSONArray("slider_images");
            for (int i = 0; i < slider_images_jsonArray.length(); i++) {
                ImagesArray.add(gson.fromJson(slider_images_jsonArray.get(i).toString(), SliderModel.class));
            }
            mPager = (ViewPager) findViewById(R.id.pager);
            mPager.setAdapter(new SlidingImage_Adapter(LanguageFragmentActivity.this, ImagesArray));
            CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.indicator);
            indicator.setViewPager(mPager);
            final float density = getResources().getDisplayMetrics().density;
            indicator.setRadius(5 * density);


            // Pager listener over indicator
            indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageSelected(int position) {
                    currentPage = position;

                }

                @Override
                public void onPageScrolled(int pos, float arg1, int arg2) {

                }

                @Override
                public void onPageScrollStateChanged(int pos) {

                }
            });
        } catch (JSONException e) {
            // Tracking exception
            MyTorismaApplication.getInstance().trackException(e);
            e.printStackTrace();
        }
    }*/

    private void slideImage(String resultString) {
        Gson gson = new Gson();
        slider = (SliderLayout) findViewById(R.id.slider);
        slider.setDuration(4000);
        slider.setDrawingCacheEnabled(true);
        slider.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_LOW);
//        slider.removeAllSliders();
        try {
            JSONObject jsonObject = new JSONObject(resultString);
            JSONArray slider_images_jsonArray = jsonObject.getJSONArray("slider_images");
            for (int i = 0; i < slider_images_jsonArray.length(); i++) {
                ImagesArray.add(gson.fromJson(slider_images_jsonArray.get(i).toString(), SliderModel.class));
                DefaultSliderView textSliderView = new DefaultSliderView(this);
                // initialize a SliderLayout
                Picasso picasso = Picasso.with(this);
                textSliderView
                        .image(ImagesArray.get(i).getImage_path())
                        .setScaleType(BaseSliderView.ScaleType.Fit)
                        .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                            @Override
                            public void onSliderClick(BaseSliderView slider1) {
                            /*mFrame = 1;
                            id = slider.getCurrentPosition();
                            mainActivity.exploreNearbyFragment.replacePlaceDetailsFragment(recommendeds.get(id).getPlace_Id(), tv_city.getText().toString());*/
                            }
                        }).setPicasso(picasso);
                slider.addSlider(textSliderView);
            }
            custom_indicator = (PagerIndicator) findViewById(R.id.indicator);
            custom_indicator.setIndicatorStyleResource(R.drawable.shape_cirlce_fill, R.drawable.shape_cirlce_unfill);
            slider.setCustomIndicator(custom_indicator);
            slider.addOnPageChangeListener(this);
        } catch (JSONException e) {
            // Tracking exception
            MyTorismaApplication.getInstance().trackException(e);
            e.printStackTrace();
        }
    }

    private void languageCall() {
        if (CommonClass.hasInternetConnection(this)) {
            String url = Constants.SERVER_URL + "json.php?action=GetLanguages";
            String json = "";
            new PostSync(LanguageFragmentActivity.this, "GetLanguages", LanguageFragmentActivity.this).execute(url, json);
        } else {
            Intent intent = new Intent(getApplicationContext(), NoInternet.class);
            startActivity(intent);
        }
    }

    public void languageResponse(String resultString) {
        if (resultString.length() > 2) {
            if (resultString.length() > 2) {
                try {
                    dbAdapter.open();
                    JSONArray jsonArray = new JSONArray(resultString);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.optJSONObject(i);
                        language = new Language();
                        language.setLan_ID(jsonObject.optString("Lan_ID"));
                        language.setLan_name(jsonObject.optString("Lan_name"));
                        language.setLan_Contents(jsonObject.optString("Lan_Contents"));
                        language.setLan_Status(jsonObject.optString("Lan_Status"));

                        JSONArray operation = jsonObject.getJSONArray("messages");
                        for (int j = 0; j < operation.length(); j++) {
                            JSONObject jsonObject2 = operation.getJSONObject(j);
                            language.setMsg_ID(jsonObject2.optString("Msg_ID"));
                            language.setMsg_Constant(jsonObject2.optString("Msg_Constant"));
                            language.setMsg_Statement(jsonObject2.optString("Msg_Statement"));
                            language.setMsg_Status(jsonObject2.optString("Msg_Status"));

                            dbAdapter.insertLanguage(language.getLan_ID(), language.getLan_name(), language.getLan_Contents(), language.getLan_Status(), language.getMsg_ID(), language.getMsg_Constant(), language.getMsg_Statement(), language.getMsg_Status());
                        }
                    }
                    dbAdapter.close();
                } catch (JSONException e) {
                    MyTorismaApplication.getInstance().trackException(e);
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnableIn);
        handler.removeCallbacks(runnableOut);
        handlerl.removeCallbacks(Update);

        if (Constants.dialog != null) {
            Constants.dialog.dismiss();
        }
    }

    @Override
    public void onResponse(String response, String action) {
        try {
            if (action.equalsIgnoreCase("GetLanguages")) {
                languageResponse(response);
            }
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.has(STATUS))
                if ((int) jsonObject.get(STATUS) == OK) {
//                    sliderImagesResponse(response);
                    slideImage(response);
                }
        } catch (Exception e) {
            // Tracking exception
            MyTorismaApplication.getInstance().trackException(e);
            Log.e(TAG, "onResponse Exception " + e.getLocalizedMessage());
        }
    }

    public String getPostDataString(JSONObject params) throws Exception {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        Iterator<String> itr = params.keys();
        while (itr.hasNext()) {
            String key = itr.next();
            Object value = params.get(key);
            if (first)
                first = false;
            else
                result.append("&");
            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));
        }
        return result.toString();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        View decorView = getWindow().getDecorView();
        if (hasFocus) {
            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    @Override
    protected void onStop() {
        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed

        super.onStop();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    public class SendPostRequest extends AsyncTask {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected Object doInBackground(Object[] params) {
            try {
                URL url = new URL("http://54.93.117.123/analytic-cms/API/increase_stand_language_cnt/" + Prefs.getString(Constants.stand_id, "")); // here is your URL path
                TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                JSONObject postDataParams = new JSONObject();
                postDataParams.put("language", Prefs.getString(Constants.lang_count, ""));
                Log.e("params", postDataParams.toString());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));
                writer.flush();
                writer.close();
                os.close();
                int responseCode = conn.getResponseCode();
                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuffer sb = new StringBuffer("");
                    String line = "";
                    while ((line = in.readLine()) != null) {
                        sb.append(line);
                        break;
                    }
                    in.close();
                    return sb.toString();
                } else {
                    Log.d("TAG", "" + responseCode);
                    return new String("false : " + responseCode);
                }
            } catch (Exception e) {
                Log.d("TAG", e.getMessage());
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
        }
    }


}
