package com.ftl.tourisma.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ftl.tourisma.R;
import com.ftl.tourisma.models.Stand_Id;

import java.util.ArrayList;

/**
 * Created by Vinay on 27-05-2017.
 */

public class StandIdAdapter extends RecyclerView.Adapter<StandIdAdapter.CategoryViewHolder> {

    public static ArrayList<Stand_Id> stand_ids = new ArrayList<>();
    Activity activity;

    public StandIdAdapter(Activity activity, ArrayList<Stand_Id> stand_ids) {
        this.stand_ids = stand_ids;
        this.activity = activity;
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.stand_id_recycler, viewGroup, false);
        return new CategoryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CategoryViewHolder CategoryViewHolder, int position) {
        CategoryViewHolder.stand_id.setText(stand_ids.get(position).getStand_id());
    }

    @Override
    public int getItemCount() {
        return stand_ids.size();
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView stand_id;

        public CategoryViewHolder(View v) {
            super(v);
            stand_id = (TextView) v.findViewById(R.id.stand_id);
        }

        @Override
        public void onClick(View view) {

        }
    }
}
