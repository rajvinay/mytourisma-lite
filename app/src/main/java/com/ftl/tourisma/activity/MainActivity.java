package com.ftl.tourisma.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ftl.tourisma.LanguageFragmentActivity;
import com.ftl.tourisma.MyTorismaApplication;
import com.ftl.tourisma.R;
import com.ftl.tourisma.custom_views.NormalTextView;
import com.ftl.tourisma.minterface.MyInterface;
import com.ftl.tourisma.utils.CommonClass;
import com.ftl.tourisma.utils.Constants;
import com.ftl.tourisma.utils.GPSTracker;
import com.ftl.tourisma.utils.Preference;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;



/**
 * Created by fipl11111 on 25-Feb-16.
 */
public class MainActivity extends FragmentActivity implements MyInterface {




    public static final String STATUS = "status";
    public static final int OK = 200;
    private static final String TAG = "YourLocationFragmentA_";
    private static PopupWindow popupWindow;
    public MainTabHostFragment mainTabHostFragment;
    public MyProfileFragment1 myProfileFragment;
    public int height;
    public int width;
    ImageView imageQrcode;
    TextView qr_link;
    boolean doubleBackToExitPressedOnce = false;
    ExploreNearbyFragment exploreNearbyFragment;
    FavouriteMainFragment favouriteFragment;
    MyProfileFragment1 profileFragment;
    private RelativeLayout imgbannerid;
    private ImageView appEnglishImageView, appRussianImageview, appArbicImageView, playEnglishImageView, playRussianImageview, playArbicImageView;
    private Object object;
    private SharedPreferences mPreferences;
    private LinearLayout llYourLocationToast, llBeaconToast;
    private Handler handler = new Handler();
    private Handler handlerBeaconToast = new Handler();
    private Runnable runnable;
    private NormalTextView txt_snack_msg, tv_login_snack, tv_sign_up_snack, tv_snack_msg;
    private LinearLayout ll_login_snack, ll_sign_up_snack;
    private SharedPreferences.Editor mEditor;
    private GPSTracker gpsTracker;
    private SearchResultPlaceDetailsFragment searchResultPlaceDetailsFragment;
    private FragmentManager fragmentManager;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    private DisplayImageOptions optionsSimple;
    private Bundle bundle;
    private Bundle bundleBeaconFromNotification;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        setContentView(R.layout.activity_home);

        appEnglishImageView = (ImageView) findViewById(R.id.app_english);
        appRussianImageview = (ImageView) findViewById(R.id.app_russian);
        appArbicImageView = (ImageView) findViewById(R.id.app_arbic);
        playEnglishImageView = (ImageView) findViewById(R.id.play_english);
        playRussianImageview = (ImageView) findViewById(R.id.play_russian);
        playArbicImageView = (ImageView) findViewById(R.id.play_arbic);

        if (Constants.language.equals("arabic")) {
            appArbicImageView.setVisibility(View.VISIBLE);
            playArbicImageView.setVisibility(View.VISIBLE);

        } else if (Constants.language.equals("russian")) {
            appRussianImageview.setVisibility(View.VISIBLE);
            playRussianImageview.setVisibility(View.VISIBLE);

        } else if (Constants.language.equals("english")) {
            appEnglishImageView.setVisibility(View.VISIBLE);
            playEnglishImageView.setVisibility(View.VISIBLE);

        }



        imgbannerid = (RelativeLayout) findViewById(R.id.imgbannerid);
        imgbannerid.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(MainActivity.this, DownloadActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

            }
        });
//
        mPreferences = getSharedPreferences(Constants.mPref, 0);
        mEditor = mPreferences.edit();
        initialisation();
        replaceMainFragment(null);

        if (Constants.mFromSelectLocation == 0) {
            //  Constants.mFromSelectLocation=0;
            getLocationOfAddress();
        }



    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        if (newConfig.orientation == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT ||
                newConfig.orientation == ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT) {
            super.onConfigurationChanged(newConfig);
        }




    }


    private void addPlaceDetailFragment(String placeId) {
        Bundle bundle = new Bundle();
        bundle.putString("placeId", placeId);
        searchResultPlaceDetailsFragment = new SearchResultPlaceDetailsFragment();
        addFragment(searchResultPlaceDetailsFragment, true, bundle);
    }

    public void replaceMainFragment(Bundle bundle) {
        mainTabHostFragment = new MainTabHostFragment();
        mainTabHostFragment.setArguments(bundle);
//        addFragment(mainTabHostFragment, true, bundle);
//        SelectLocationFragment searchResultFragmentFragment = new SelectLocationFragment();
//        replaceFragment(searchResultFragmentFragment, true, null);
        fragmentManager.beginTransaction().replace(R.id.frame_container, mainTabHostFragment, MainTabHostFragment.class.getSimpleName()).commit();

    }

//    private void callApiForGetBeacons() {
//        if (CommonClass.hasInternetConnection(this)) {
//            String url = Constants.SERVER_URL + "json.php?action=Beacons";
//            String json = "[{\"Lan_Id\":\"" + mPreferences.getString("Lan_Id", "") + "\"}]";
//
//            new PostSync(MainActivity.this, "Beacons", MainActivity.this).execute(url, json);
//        } else {
//            SnackbarManager.show(Snackbar.with(this).color(Utilities.getColor(this, R.color.mBlue)).text("No Internet connection!"));
//        }
//    }

    public void addFragment(Fragment fragment, boolean addToBackStack, Bundle bundle) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
//        transaction.setCustomAnimations(R.anim.left_in_fragment, R.anim.left_out_fragment, R.anim.right_in_fragment, R.anim.right_out_fragment);
        String backStateName = fragment.getClass().getName();
        if (addToBackStack) {
            transaction.addToBackStack(backStateName);
        }
        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
        if (!fragmentPopped && fragmentManager.findFragmentByTag(backStateName) == null) {
            if (bundle != null) {
                fragment.setArguments(bundle);
            }
            transaction.add(R.id.frame_container, fragment, backStateName);
            transaction.commit();
        }
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
    }

    public void addFragmentFromBottom(Fragment fragment, boolean addToBackStack, Bundle bundle) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
//        transaction.setCustomAnimations(R.anim.push_uppp_in, R.anim.push_up_out_no, 0, 0);

        String backStateName = fragment.getClass().getName();

        if (addToBackStack) {
            transaction.addToBackStack(backStateName);
        }
        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
        if (!fragmentPopped && fragmentManager.findFragmentByTag(backStateName) == null) {
            if (bundle != null) {
                fragment.setArguments(bundle);
            }
            transaction.add(R.id.frame_container, fragment, backStateName);
            transaction.commit();
        }
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
    }


    public void addFragmentZoom(Fragment fragment, boolean addToBackStack, Bundle bundle) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
//        transaction.setCustomAnimations(R.anim.zoom_in, R.anim.translate, R.anim.translate, R.anim.fade_out);

        String backStateName = fragment.getClass().getName();

        if (addToBackStack) {
            transaction.addToBackStack(backStateName);
        }
        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
        if (!fragmentPopped && fragmentManager.findFragmentByTag(backStateName) == null) {
            if (bundle != null) {
                fragment.setArguments(bundle);
            }
            transaction.add(R.id.frame_container, fragment, backStateName);
            transaction.commit();
        }
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
    }

    public void getLocationOfAddress() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        gpsTracker = new GPSTracker(this, true);
        if (CommonClass.hasInternetConnection(this)) {
     /*       if (Prefs.getString(Constants.first_time, "").equals("first")) {
                mEditor.putString("latitude1", "23.424076").commit();
                mEditor.putString("longitude1", "53.847818").commit();
                mEditor.putString("latitude2", "23.424076").commit();
                mEditor.putString("longitude2", "53.847818").commit();
                mPreferences.edit().putString(Preference.Pref_City, "United Arab Emirates").apply();
                Prefs.putString(Constants.first_time, "second");
            } else {*/
            if (gpsTracker.getLatitude() == 0 || gpsTracker.getLongitude() == 0) {
                mEditor.putString("latitude1", "23.424076").commit();
                mEditor.putString("longitude1", "53.847818").commit();
                mEditor.putString("latitude2", "23.424076").commit();
                mEditor.putString("longitude2", "53.847818").commit();
                mPreferences.edit().putString(Preference.Pref_City, "United Arab Emirates").apply();

            } else {
                mEditor.putString("latitude1", String.valueOf(gpsTracker.getLatitude())).commit();
                mEditor.putString("longitude1", String.valueOf(gpsTracker.getLongitude())).commit();
                mEditor.putString("latitude2", String.valueOf(gpsTracker.getLatitude())).commit();
                mEditor.putString("longitude2", String.valueOf(gpsTracker.getLongitude())).commit();
                getGeoLocation();


//            Constants.latitude = String.valueOf(gpsTracker.getLatitude());
//            Constants.longitude = String.valueOf(gpsTracker.getLongitude());

                Log.d("System out", "Constant.latitude1 " + mPreferences.getString("latitude1", ""));
                Log.d("System out", "Constant.longitude1 " + mPreferences.getString("longitude1", ""));

            }
//            }
        } else {
            Intent intent = new Intent(getApplicationContext(), NoInternet.class);
            startActivity(intent);
            //SnackbarManager.show(Snackbar.with(this).color(getResources().getColor(R.color.mBlue)).text(Constants.showMessage(this, mPreferences.getString("Lan_Id", ""), "NOINTERNET")));
        }
    }

    private void initialisation() {

        fragmentManager = getSupportFragmentManager();

        TextView textdownloadid = (TextView) findViewById(R.id.textdownloadid);


        textdownloadid.setText(Constants.showMessage(MainActivity.this, mPreferences.getString("Lan_Id", ""), "download_app"));


        llYourLocationToast = (LinearLayout) findViewById(R.id.llYourLocationToast);
        ll_login_snack = (LinearLayout) findViewById(R.id.ll_login_snack);
        ll_sign_up_snack = (LinearLayout) findViewById(R.id.ll_sign_up_snack);
        tv_login_snack = (NormalTextView) findViewById(R.id.tv_login_snack);
        llBeaconToast = (LinearLayout) findViewById(R.id.llBeaconToast);
        txt_snack_msg = (NormalTextView) findViewById(R.id.txt_snack_msg);
        tv_sign_up_snack = (NormalTextView) findViewById(R.id.tv_sign_up_snack);
        tv_snack_msg = (NormalTextView) findViewById(R.id.tv_snack_msg);


        try {
            DisplayMetrics displaymetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            height = displaymetrics.heightPixels;
            width = displaymetrics.widthPixels;
        } catch (Exception e) {
            // Tracking exception
            MyTorismaApplication.getInstance().trackException(e);
            e.printStackTrace();
        }

        mPreferences = this.getSharedPreferences(Constants.mPref, 0);
        mEditor = mPreferences.edit();
/*
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .build();*/

        imageLoader = ImageLoader.getInstance();

        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(null)
                .displayer(new RoundedBitmapDisplayer(10))
                .showImageOnFail(null).cacheInMemory(true)
                .cacheOnDisk(true).build();
        optionsSimple = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(null)
                .displayer(new SimpleBitmapDisplayer())
                .showImageOnFail(null).cacheInMemory(true)
                .cacheOnDisk(true).build();

    }

    public SharedPreferences getPreferences() {
        if (mPreferences == null) {
            mPreferences = this.getSharedPreferences(Constants.mPref, 0);
        }
        return mPreferences;
    }

    @Override
    protected void onResume(){
        super.onResume();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                final Intent mainIntent = new Intent(MainActivity.this, LanguageFragmentActivity.class);
                MainActivity.this.startActivity(mainIntent);
                MainActivity.this.finish();
            }
        }, 600000);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
//            unregisterReceiver(broadcastReceiver);
        } catch (Exception e) {
            // Tracking exception
            MyTorismaApplication.getInstance().trackException(e);
            e.printStackTrace();
        }
    }


    @Override
    public void backToHome(Object object) {
//        this.object = object;
//        Log.d("System out", "object " + object.toString());
    }

    @Override
    public void showGuestSnackToast() {
        guestSnackToast();
    }

    public void getGeoLocation() {
        try {
            Geocoder geo = new Geocoder(this.getApplicationContext(), Locale.getDefault());
            List<Address> addresses = geo.getFromLocation(Double.parseDouble(mPreferences.getString("latitude2", "")), Double.parseDouble(mPreferences.getString("longitude2", "")), 1);
            if (addresses.isEmpty()) {
            } else {
                if (addresses.size() > 0) {
                    String str = (addresses.get(0).getAddressLine(1) + ", " + addresses.get(0).getLocality() + ", "
                            + addresses.get(0).getAdminArea() + ", " + addresses.get(0).getCountryName());

                    String umAddress1 = addresses.get(0).getAddressLine(0);
                    String city = addresses.get(0).getLocality();
                    mEditor.putString(Preference.Pref_City, city).commit();
                    String state = addresses.get(0).getAdminArea();
                    mEditor.putString(Preference.Pref_State, state).commit();
                    String zipCode = addresses.get(0).getPostalCode();
                    String country = addresses.get(0).getCountryName();
                    mEditor.putString(Preference.Pref_Country, country).commit();

                    String addressmaps = str.replaceAll(" null,", "");
                    // flag = 0;
                    //  etSearch.setHint(addressmaps);

                    /*Intent mIntent = new Intent(SelectLocationFragmentActivity.this, YourLocationFragmentActivity.class);
                    startActivity(mIntent);
                    finish();*/

                    Log.i("System out", "Get current location city--> " + city);
                    Log.i("System out", "Get current location state--> " + state);
                    Log.i("System out", "Get current location country--> " + country);
                }
            }

        } catch (Exception e) {
            // Tracking exception
            MyTorismaApplication.getInstance().trackException(e);
            e.printStackTrace();

            String address = "";
            String apiRequest = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + mPreferences.getString("latitude1", "") + "," + mPreferences.getString("longitude1", ""); //+ "&ka&sensor=false"
            HttpGet httpGet = new HttpGet(apiRequest);
            HttpClient client = new DefaultHttpClient();
            HttpResponse response;
            StringBuilder stringBuilder = new StringBuilder();

            try {
                response = client.execute(httpGet);
                HttpEntity entity = response.getEntity();
                InputStream stream = entity.getContent();
                int b;
                while ((b = stream.read()) != -1) {
                    stringBuilder.append((char) b);
                }
            } catch (ClientProtocolException e1) {
                e1.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject = new JSONObject(stringBuilder.toString());
                address = jsonObject.getString("formatted_address");
            } catch (JSONException e3) {
                e.printStackTrace();
            }

        }
    }

    /*
    Managing response of Beacon api
     */
    public void getBeaconsResponse(String resultString) {
        if (resultString != null && resultString.length() > 2) {
            try {
                Set<String> set = new HashSet<String>();
                ArrayList<String> beaconsArrayList = new ArrayList<>();

                JSONArray jsonArray = new JSONArray(resultString);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.optJSONObject(i);
                    beaconsArrayList.add(jsonObject.toString());
                }
                set.addAll(beaconsArrayList);
                Preference.setStringSetPrefs("keyBeacons", this, set);
                //scoreEditor.commit();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }



   /* public void replaceSearchResultFragment(ArrayList<Nearby> nearbies1, String search) {
        SearchResultFragmentFragment searchResultFragmentFragment = SearchResultFragmentFragment.NewInstance(nearbies1, search);
//        replaceFragment(searchResultFragmentFragment, true, null);
        fragmentManager.beginTransaction().replace(R.id.frame_container, searchResultFragmentFragment, SearchResultFragmentFragment.class.getSimpleName()).addToBackStack(SearchResultFragmentFragment.class.getSimpleName()).commit();
    }*/

    public void popbackStackFragment() {
        try {
            fragmentManager.popBackStack();
        } catch (Exception e) {
            // Tracking exception
            MyTorismaApplication.getInstance().trackException(e);
            e.printStackTrace();
        }
    }

    public void replaceFragment(Fragment fragment, boolean
            addToBackStack, Bundle bundle) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
//        transaction.setCustomAnimations(R.anim.left_in_fragment, R.anim.left_out_fragment, R.anim.right_in_fragment, R.anim.right_out_fragment);
        String backStateName = fragment.getClass().getName();
        if (addToBackStack) {
            transaction.addToBackStack(backStateName);
        }

        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
        if (!fragmentPopped && fragmentManager.findFragmentByTag(backStateName) == null) {
            if (bundle != null) {
                fragment.setArguments(bundle);
            }
            transaction.replace(R.id.frame_container, fragment, backStateName);
            transaction.commit();
        }
    }

    @Override
    public void onBackPressed() {


        Intent intent = new Intent(MainActivity.this, LanguageFragmentActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
//        if (mainTabHostFragment.vpFragment.getCurrentItem() == 2) {
//            if (mPreferences.getString("User_Id", "").equalsIgnoreCase("0")) {
//                mainTabHostFragment.vpFragment.setCurrentItem(0);
//            } else {
//                mainTabHostFragment.vpFragment.setCurrentItem(1);
//            }
//        } else if (mainTabHostFragment.vpFragment.getCurrentItem() == 1) {
//
//            FragmentManager fm = getSupportFragmentManager();
//            if (fm.getBackStackEntryCount() == 0) {
//                mainTabHostFragment.vpFragment.setCurrentItem(0);
//
//            } else {
//                super.onBackPressed();
//            }
//
//        } else {
//            if (mainTabHostFragment.vpFragment.getCurrentItem() == 0) {
//
//            }
//            FragmentManager fm = getSupportFragmentManager();
//            if (fm.getBackStackEntryCount() == 0) {
//                if (doubleBackToExitPressedOnce) {
//                    super.onBackPressed();
//                    return;
//                }
//
//                this.doubleBackToExitPressedOnce = true;
//                SnackbarManager.show(Snackbar.with(MainActivity.this).color(Utils.getColor(this, R.color.mBlue)).text(Constants.showMessage(MainActivity.this, mPreferences.getString("Lan_Id", ""), "Please click BACK again to exit")));
//
//                new Handler().postDelayed(new Runnable() {
//
//                    @Override
//                    public void run() {
//                        doubleBackToExitPressedOnce = false;
//                    }
//                }, 2000);
//                //onCloseApp("Are you sure you want to close the App?");
//            } else {
//                //for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
////                    fm.popBackStack();
//                // }
//                super.onBackPressed();
    }
            /*if (object != null) {
                Constants.mStaticNearCall = 0;
                ((ExploreNearbyFragment1) object).update();
                object = null;
//                Log.d("System out", "home.....");
            } else {
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();
                    return;
                }

                this.doubleBackToExitPressedOnce = true;
                SnackbarManager.show(Snackbar.with(MainActivity.this).color(Utils.getColor(this, R.color.mBlue)).text(Constants.showMessage(MainActivity.this, mPreferences.getString("Lan_Id", ""), "Please click BACK again to exit")));

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            }*/


    public void guestSnackToast() {

        tv_login_snack.setText(Constants.showMessage(MainActivity.this, mPreferences.getString("Lan_Id", ""), "Login"));
        tv_sign_up_snack.setText(Constants.showMessage(MainActivity.this, mPreferences.getString("Lan_Id", ""), "SignUp"));
        tv_snack_msg.setText(Constants.showMessage(MainActivity.this, mPreferences.getString("Lan_Id", ""), "GetStarted"));

        runnable = new Runnable() {
            @Override
            public void run() {
                llYourLocationToast.setVisibility(View.GONE);
            }
        };
        llYourLocationToast.setVisibility(View.VISIBLE);
        handler.postDelayed(runnable, 4000);
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        View decorView = getWindow().getDecorView();
        if (hasFocus) {
            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

//    @Override
//    public void onResponse(String response, String action) {
//        try {
//            JSONObject jsonObject = new JSONObject(response);
//            if (jsonObject.has(STATUS))
//                if ((int) jsonObject.get(STATUS) == OK)
//                    qrcode_Response(response);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public void qrcode_call() {
//        if (CommonClass.hasInternetConnection(getApplicationContext())) {
//            String url = "http://54.93.117.123/analytic-cms/API/get_qr_for_stand/" + Prefs.getString(Constants.stand_id, "");
//            String json = "";
//            new PostSync(getApplicationContext(), "qrCode", MainActivity.this).execute(url, json);
//        } else {
//            Intent intent = new Intent(getApplicationContext(), NoInternet.class);
//            startActivity(intent);
//        }
//    }
//
//    public void qrcode_Response(String resultString) {
//        try {
//            JSONObject jsonObject = new JSONObject(resultString);
//            String image = jsonObject.getString("image_path");
//            Picasso.with(MainActivity.this)
//                    .load(image)
//                    .into(imageQrcode);
//
//            qr_link.setText(jsonObject.getString("url"));
//
//        } catch (JSONException e) {
//            // Tracking exception
//            MyTorismaApplication.getInstance().trackException(e);
//            e.printStackTrace();
//        }
//    }

    public class PagerAdapter extends FragmentPagerAdapter {

        private List<Fragment> fragments;

        public PagerAdapter(FragmentManager fm, List<Fragment> fragments) {
            super(fm);
            this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    exploreNearbyFragment = new ExploreNearbyFragment();
                    return exploreNearbyFragment;

                case 1:
                    //if (profileFragment == null)
                    profileFragment = new MyProfileFragment1();
                    //else
                    return profileFragment;
                // break;
            }
            return this.fragments.get(position);
        }

        @Override
        public int getCount() {
            return this.fragments.size();
        }


    }


}