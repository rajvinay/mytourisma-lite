package com.ftl.tourisma.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ftl.tourisma.LanguageFragmentActivity;
import com.ftl.tourisma.MyTorismaApplication;
import com.ftl.tourisma.R;
import com.ftl.tourisma.postsync.PostSync;
import com.ftl.tourisma.postsync.post_sync;
import com.ftl.tourisma.utils.CommonClass;
import com.ftl.tourisma.utils.Constants;
import com.pixplicity.easyprefs.library.Prefs;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by VirtualDusk on 31-May-17.
 */

public class DownloadActivity extends FragmentActivity implements post_sync.ResponseHandler {

    public static final String STATUS = "status";
    public static final int OK = 200;
    TextView qr_link;
    ImageView backqr, crossqr, imageQrcode;
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.download_view);
        mPreferences = getSharedPreferences(Constants.mPref, 0);
        mEditor = mPreferences.edit();

        backqr = (ImageView) findViewById(R.id.backqr);
        crossqr = (ImageView) findViewById(R.id.crossbuttonqr);
        qr_link = (TextView) findViewById(R.id.qr_link);
        TextView textvisit = (TextView) findViewById(R.id.textvisit);
        textvisit.setText(Constants.showMessage(DownloadActivity.this, getPreferences().getString("Lan_Id", ""), "Visit Link"));
        TextView textscan = (TextView) findViewById(R.id.textsc);
        textscan.setText(Constants.showMessage(DownloadActivity.this, getPreferences().getString("Lan_Id", ""), "Or Scan"));
        TextView botomtext = (TextView) findViewById(R.id.botomtext);
        botomtext.setText(Constants.showMessage(DownloadActivity.this, getPreferences().getString("Lan_Id", ""), "Problems? call us on"));
        TextView textor = (TextView) findViewById(R.id.textor);
        textor.setText(Constants.showMessage(DownloadActivity.this, getPreferences().getString("Lan_Id", ""), "or"));

        //Qr-code
        imageQrcode = (ImageView) findViewById(R.id.qrcode);

        qrcode_call();

        backqr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });
        crossqr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DownloadActivity.this, LanguageFragmentActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

            }
        });

    }

    public SharedPreferences getPreferences() {
        if (mPreferences == null) {
            mPreferences = this.getSharedPreferences(Constants.mPref, 0);
        }
        return mPreferences;
    }

    @Override
    public void onResponse(String response, String action) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.has(STATUS))
                if ((int) jsonObject.get(STATUS) == OK)
                    qrcode_Response(response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void qrcode_call() {
        if (CommonClass.hasInternetConnection(getApplicationContext())) {
            String url = "http://54.93.117.123/analytic-cms/API/get_qr_for_stand/" + Prefs.getString(Constants.stand_id, "");
            String json = "";
            new PostSync(getApplicationContext(), "qrCode", DownloadActivity.this).execute(url, json);
        } else {
            Intent intent = new Intent(getApplicationContext(), NoInternet.class);
            startActivity(intent);
        }
    }

    public void qrcode_Response(String resultString) {
        try {
            JSONObject jsonObject = new JSONObject(resultString);
            String image = jsonObject.getString("image_path");
            Picasso.with(DownloadActivity.this)
                    .load(image)
                    .into(imageQrcode);


            String urldata = jsonObject.getString("url");
            String[] items = urldata.split("http://www.");
            for (String item : items) {
                qr_link.setText(item);
            }





        } catch (JSONException e) {
            // Tracking exception
            MyTorismaApplication.getInstance().trackException(e);
            e.printStackTrace();
        }
    }

}
