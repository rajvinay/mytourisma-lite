package com.ftl.tourisma.activity;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;

import com.ftl.tourisma.LanguageFragmentActivity;
import com.ftl.tourisma.MyTorismaApplication;
import com.ftl.tourisma.R;
import com.ftl.tourisma.adapters.StandIdAdapter;
import com.ftl.tourisma.models.Stand_Id;
import com.ftl.tourisma.postsync.PostSync;
import com.ftl.tourisma.postsync.post_sync;
import com.ftl.tourisma.utils.CommonClass;
import com.ftl.tourisma.utils.Constants;
import com.ftl.tourisma.utils.RecyclerItemClickListener;
import com.google.gson.Gson;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Vinay on 27-05-2017.
 */

public class SelectStandId extends FragmentActivity implements post_sync.ResponseHandler {

    public static final String STATUS = "status";
    public static final int OK = 200;
    RecyclerView stand_id_recycler_view;
    ArrayList<Stand_Id> stand_ids = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!Prefs.getString(Constants.stand_id, "").equals("")) {
            Intent intent = new Intent(getApplicationContext(), LanguageFragmentActivity.class);
            startActivity(intent);
            finish();
        } else {
            setContentView(R.layout.stand_id_layout);
            dcl_layout_variables();
            onClickListners();
            getStandIds();

            //Initializing layout manager
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            stand_id_recycler_view.setLayoutManager(linearLayoutManager);
        }
    }

    public void dcl_layout_variables() {
        stand_id_recycler_view = (RecyclerView) findViewById(R.id.stand_id_recycler_view);
    }

    public void onClickListners() {

        stand_id_recycler_view.addOnItemTouchListener(new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Prefs.putString(Constants.stand_id, StandIdAdapter.stand_ids.get(position).getStand_id());
                        try {
                            new SendPostRequest().execute();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Intent mIntent = new Intent(getApplicationContext(), LanguageFragmentActivity.class);
                        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(mIntent);
                        finish();
                    }
                })
        );
    }

    public void getStandIds() {
        if (CommonClass.hasInternetConnection(getApplicationContext())) {
            String url = "http://54.93.117.123/analytic-cms/API/get_not_assigned_stand";
            String json = "";
            new PostSync(getApplicationContext(), "StandId", SelectStandId.this).execute(url, json);
        } else {
            Intent intent = new Intent(getApplicationContext(), NoInternet.class);
            startActivity(intent);
        }
    }

    @Override
    public void onResponse(String response, String action) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.has(STATUS))
                if ((int) jsonObject.get(STATUS) == OK)
                    getStandIdResponse(response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getStandIdResponse(String resultString) {
        Gson gson = new Gson();
        stand_ids.clear();
        try {
            JSONObject jsonObject = new JSONObject(resultString);
            JSONArray stand_id_jsonArray = jsonObject.getJSONArray("stands");
            for (int i = 0; i < stand_id_jsonArray.length(); i++) {
                stand_ids.add(gson.fromJson(stand_id_jsonArray.get(i).toString(), Stand_Id.class));
            }
            stand_id_recycler_view.setAdapter(new StandIdAdapter(SelectStandId.this, stand_ids));
        } catch (JSONException e) {
            // Tracking exception
            MyTorismaApplication.getInstance().trackException(e);
            e.printStackTrace();
        }
    }

    public String getPostDataString(JSONObject params) throws Exception {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        Iterator<String> itr = params.keys();
        while (itr.hasNext()) {
            String key = itr.next();
            Object value = params.get(key);
            if (first)
                first = false;
            else
                result.append("&");
            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));
        }
        return result.toString();
    }

    public class SendPostRequest extends AsyncTask {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected Object doInBackground(Object[] params) {
            try {
                URL url = new URL("http://54.93.117.123/analytic-cms/API/add_device_for_stand"); // here is your URL path
                TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                JSONObject postDataParams = new JSONObject();
                postDataParams.put("stand_id", Prefs.getString(Constants.stand_id, ""));
                postDataParams.put("device_IMEI", telephonyManager.getDeviceId());
                Log.e("params", postDataParams.toString());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));
                writer.flush();
                writer.close();
                os.close();
                int responseCode = conn.getResponseCode();
                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuffer sb = new StringBuffer("");
                    String line = "";
                    while ((line = in.readLine()) != null) {
                        sb.append(line);
                        break;
                    }
                    in.close();
                    return sb.toString();
                } else {
                    Log.d("TAG", "" + responseCode);
                    return new String("false : " + responseCode);
                }
            } catch (Exception e) {
                Log.d("TAG", e.getMessage());
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
        }
    }
}

