package com.ftl.tourisma.models;

/**
 * Created by Vinay on 27-05-2017.
 */

public class Stand_Id {

    private String stand_id;

    public String getStand_id() {
        return stand_id;
    }

    public void setStand_id(String stand_id) {
        this.stand_id = stand_id;
    }
}
