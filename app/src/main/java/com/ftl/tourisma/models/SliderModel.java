package com.ftl.tourisma.models;

/**
 * Created by VirtualDusk on 5/26/2017.
 */

public class SliderModel {

    private String image_path;
    private String image_no;

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }

    public String getImage_no() {
        return image_no;
    }

    public void setImage_no(String image_no) {
        this.image_no = image_no;
    }
}
